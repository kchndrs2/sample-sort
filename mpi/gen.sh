#!/bin/bash

for i in `seq 16 24`; do
  for j in `seq 1 3`; do
    for k in `seq 1 3`; do
      if [ $j == 3 ] && [ $k == 1 ]
      then
        echo Uniform255 QuickSort Skipped
      else 
        echo running
        mpirun -n 1 ./a.out $i $j $k
        mpirun -n 2 ./a.out $i $j $k
        mpirun -n 4 ./a.out $i $j $k
        mpirun -n 8 ./a.out $i $j $k
        mpirun -n 16 ./a.out $i $j $k
      fi
    done
  done 
done

